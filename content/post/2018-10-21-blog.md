---
title: Week Eight
date: 2018-10-21
---

# What did you do this past week?

This past week, our group set up the backend of our site. I spent a lot of time building Python tools
to consume the RESTful APIs we chose for our models. We're taking a slower pace than we're used to
since we have to learn what each tool is and how it works as we're trying to implement a solution using it.
It was also midterm week for just about everyone at UT, which meant every free minute was spent cramming for 
classes.


# What's in your way?

None of our group has used React before, and we've saved it for last. It's time that we finally bite the bullet
and start using it, even though it will probably be painful at first. Also, I have an essay I've put off until this week
because I have to be in the right [creative mindset](https://www.gocomics.com/calvinandhobbes/1992/05/21).

# What will you do next week?

Next week I'll be starting React with the rest of my group to get the project finished for next Tuesday's deadline.
I'll have to juggle these responsibilites with those from my other classes to make sure everything gets done when it
needs to be done.

# What was your experience of Test #1 (the problems, the time, HackerRank)?

I felt that Test 1 was a fair assessment of what has been in lecture so far. Though the harder problems were loaded up front,
Downing did warn the room that the questions 'might' not be arranged in ascending difficulty. I think the hardest part of Test 1
was the time. The questions themselves were not impossibly difficult, but the stress of having to complete them in the alloted time
rattled quite a few people (including me at first).

# What's your pick-of-the-week or tip-of-the-week?

There's a great app that corrects mistypings of commands in the console, if a little vulgar, called ["f**k"](https://github.com/nvbn/thefuck).
It's perfect if you've got sticky fingers like me and is probably most useful (and fun) while pair programming. Also, if the language is discouraging,
you can always alias it to "darn-it!"