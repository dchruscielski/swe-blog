---
title: The Ethical Software Engineer
date: 2018-10-30
---

The best action to take when ordered to find the employee who left an anonymous confession is to refuse
to oust the individual.

The categorical imperative, a major normative ethical theory, states that refusing to find the employee is the only morally permissible decision.
First coined by Kant, this theory basically states that we have a duty to perform an action only if it could be used as a universal rule and still be moral.
The question raised by the more generalized action here is, "Is it morally permissible to break a promise that would no longer benefit me to keep?"
The answer should obviously be no. If everyone could break a promise they made when it no longer suited them to keep it,
society would be much worse than it is now. The trust that comes from honoring a promise would break down, and cooperation would slow down if it wouldn't
stop completely. Because it is wrong to break a promise, I should not break the company's promise of anonymity. I am operating
as a part of the company, so in my official capacity I am acting as the company, meaning I take responsibility for upholding the promise made.

A more utilitarian argument could claim that the utility gained by finding and removing a saboteur outweighs the cost of breaking the company's promise.
This employee is damaging the parts, but they are also damaging the reputation of the company, not to mention their profits. This damage is also continuous -
The longer the employee continues unchecked, the greater the damage done. Breaking this promise would be a one-time event compared to a long term sabotage campaign.
Finding and apprehending the employee could be considred justice. After all, he or she is doing something wrong that is a fireable offense, and the only reason he or she
has not been fired already is because they remain anonymous. Furthermore, if I refuse to find the employee, I could be punished in their place, even though I have done
nothing wrong. All of these considerations could tip the scale in favor of breaking the promise and revealing the employee.

However, I'll argue that a utilitarian view would still find greater utility in not unmasking the employee (as long as the company is not manufacturing
components that could cause great harm by distributing faulty parts). It is true that finding and firing the employee would make considerable gains for the company as an abstract corporation.
They keep a higher standard of quality and spend less on reimbursing faulty parts. But the company as a group of people would suffer in the long term. The trust of the employees 
would be decimated. They would discover that they had been betrayed by a fellow employee who sabotaged their collective work, but more importantly, they were betrayed by their superiors
who broke a direct promise made to them when the company no longer found it convenient to be honest. The employees would have lower morale at work, be less trusting of their superiors and their peers,
and less likely to be direct and honest with others at work. The hit taken across all the employees of the company would outweigh the benefit of breaking the rules to remove the saboteur.

Using two of the most common ethical theories arguably still comes to the conclusion that I should refuse to find the rogue employee if I want to act in a morally permissible way.