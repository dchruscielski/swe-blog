---
title: Final Entry
date: 2018-12-11
---

# What did you like the least about the class?

I knew that there would be a big disconnect between Downing's lectures and the projects;
he told us that from the very beginning. However, I was surprised at just how heavily 
Javascript, and more specifically ReactJS, is used for the projects, and we didn't
talk about Javascript at all over the semester.

# What did you like the most about the class?

The projects in this class are incredibly satisfying. You can see the results of what
your work is doing much more tangibly than a lot of other classes, and it's something that
even a non-CS person can appreciate. The lectures are also engaging and easy to follow,
and those lectures changed my opinion of Python for the better.

# What's the most significant thing you learned?

Taking the plunge to deploy something on AWS really broke the ice for me as far as hosting some web project.
 I had always wanted to do something like that before, but I had been intimidated by the process. Having to do it
for a class gave me the push I needed to just do it.

# How many hours a week did you spend coding/debugging/testing for this class?

It depends week-by-week, especially in a group where we put off segments until the last
minute. At its worst, it was probably 30 hours a week; at its best, it was probably 5.

# How many hours a week did you spend reading/studying for this class?

Towards the beginning of the semester, I spent a few hours a week reading all the required readings
before lectures. By the end, I only went over the example code Downing posts.

# How many lines of code do you think you wrote?

By my best guess, a couple of thousands of lines, though not all those lines were useful or stayed until
the finished product.

# What required tool did you not know and now find very useful?

I had no experience with React before this class, and I'm glad I got some exposure to it.

# What's the most useful Web dev tool that your group used that was not required?

We used Enzyme for our React testing to help testing individual components.

# If you could change one thing about the course, what would it be?

I would change the requirement to have the topic of our projects focused on
civic engagement. It is difficult to find a topic and model instances that fit the
criteria and are not already taken by another group, especially since the number of
APIs that meet the requirements are limited.