---
title: Week Nine
date: 2018-10-28
---

# What did you do this past week?

This week my team finished up the backend of our project, making sure
that we had the right data to fit the requirements as opposed to just having
data in the backend. My team and I spent the bulk of this week building React
components for our frontend. Though it was a very slow process as we tentatively wrote
our framework, it's incredibly satisfying to watch the site become something dynamic
and somehow alive. Also, we had an Infowars article show up somehow. I'm sure there's a topical
comparison to be made somewhere in there.

# What's in your way?

We need to finish up lingering elements of Phase II, including the unit and acceptance tests.
It's odd to feel like you're done and yet still have important work to do. Registration for
next semester's classes divides everyone's time as they stress about what lies ahead.

# What will you do next week?

Next week, we'll finish Phase II. I'll still be working on unit tests for our backend Python and preparing 
the technical report for the project. I'll take yet another chemistry exam (do they ever end?) and register for classes (including Chem II, so no).

# What did you think of the talk by Dr. Rich and Dr. Cline?

Their talk raised a lot of good questions that apply to any job, not just software engineering.
It's unnerving to realise how many of the systems we rely on to guide us, whether the law, our
superiors, or our own intuition, can be wrong. They raised many questions which I hope they will
follow up with good answers. In any case, they demonstrated just how important philosophy is for the
important moments in our lives.

# What's your pick-of-the-week or tip-of-the-week?

[nose2](https://nose2.readthedocs.io/en/latest/index.html) is a helpful tool for unit testing in Python that builds off of 
Python's built-in unittest. It will automatically detect your unit tests and execute them all in one command, making it easy
to run and ensure that you get full coverage across all of your code. You can include this in your CI to extend your tests without changing the .yml
